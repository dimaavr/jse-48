package ru.tsc.avramenko.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.endpoint.Project;
import ru.tsc.avramenko.tm.endpoint.ProjectDTO;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(@Nullable ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().value());
        System.out.println("Created: " + project.getCreated());
        System.out.println("Start Date: " + project.getStartDate());
        System.out.println("Finish Date: " + project.getFinishDate());
    }

}